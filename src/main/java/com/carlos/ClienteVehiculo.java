package com.carlos;

import com.carlos.constructor.abstrato.ConstructorDocumentacionVehiculo;
import com.carlos.constructor.concreto.ConstructorDocumentacionVehiculoHtml;
import com.carlos.constructor.concreto.ConstructorDocumentacionVehiculoPdf;
import com.carlos.director.Director;
import com.carlos.producto.Documentacion;

public class ClienteVehiculo {

    public static void main(String[] args) {
        // 1  para pdf  y  2 para html
        String  tiposDocumento = "1";
        ConstructorDocumentacionVehiculo documentacionVehiculo = null;

        if(tiposDocumento.equals("1"))
            documentacionVehiculo = new ConstructorDocumentacionVehiculoPdf();
        if (tiposDocumento.equals("2"))
            documentacionVehiculo =new ConstructorDocumentacionVehiculoHtml();

        Director director = new Director(documentacionVehiculo);
        Documentacion documentacion = director.construye("Carlos Arturo Gonzalez");
        documentacion.imprimir();

    }
}
