package com.carlos.constructor.abstrato;

import com.carlos.producto.Documentacion;

public abstract class ConstructorDocumentacionVehiculo {

    protected Documentacion documentacion;

    public abstract  void  construyeSolicitudPedido( String  nombreCliente);
    public abstract  void  construyeSolicitudMatricula( String  nombreSolicitante);

    public  Documentacion resutado(){
         return  documentacion;
    }

}
