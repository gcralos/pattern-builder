package com.carlos.constructor.concreto;

import com.carlos.constructor.abstrato.ConstructorDocumentacionVehiculo;
import com.carlos.producto.DocumentoHtml;

public class ConstructorDocumentacionVehiculoHtml extends ConstructorDocumentacionVehiculo {

    public ConstructorDocumentacionVehiculoHtml(){
        documentacion = new DocumentoHtml();
    }

    @Override
    public void construyeSolicitudPedido(String nombreCliente) {

        StringBuilder documento = new StringBuilder();
        documento.append("<HTML>");
        documento.append("Solicitud  de pedido cliente");
        documento.append(nombreCliente);
        documento.append("</HTML>");
        documentacion.agregarDocumento(String.valueOf(documento));

    }

    @Override
    public void construyeSolicitudMatricula(String nombreSolicitante) {

        StringBuilder documento = new StringBuilder();
        documento.append("<HTML>");
        documento.append("Solicitud  de matricula  cliente");
        documento.append(nombreSolicitante);
        documento.append("</HTML>");
        documentacion.agregarDocumento(String.valueOf(documento));

    }
}
