package com.carlos.producto;

public class DocumentoPdf extends Documentacion {


    @Override
    public void agregarDocumento(String documento) {
        if(documento.startsWith("<PDF>"))
            contenido.add(documento);
    }

    @Override
    public void imprimir() {
        System.out.printf("---- Documentacion  PDF ---");
        for (String s: contenido)
            System.out.printf(s);

    }
}
