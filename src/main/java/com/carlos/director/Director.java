package com.carlos.director;

import com.carlos.constructor.abstrato.ConstructorDocumentacionVehiculo;
import com.carlos.producto.Documentacion;

public class Director {

    protected ConstructorDocumentacionVehiculo constructor;

    public Director(ConstructorDocumentacionVehiculo constructor) {
        this.constructor = constructor;
    }

    public Documentacion construye( String nombreCliente){

        constructor.construyeSolicitudPedido(nombreCliente);
        constructor.construyeSolicitudMatricula(nombreCliente);
        Documentacion documentacion = constructor.resutado();
        return documentacion;
    }
}
