package com.carlos.producto;

public class DocumentoHtml extends  Documentacion {

    @Override
    public void agregarDocumento(String docuemnto) {
        if(docuemnto.startsWith("<HTML>"))
            contenido.add(docuemnto);
    }

    @Override
    public void imprimir() {
        System.out.printf("---- Documentacion  HTML ----");
        for (String s: contenido)
            System.out.printf(s);
    }
}
