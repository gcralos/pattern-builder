package com.carlos.constructor.concreto;

import com.carlos.constructor.abstrato.ConstructorDocumentacionVehiculo;
import com.carlos.producto.DocumentoPdf;

public class ConstructorDocumentacionVehiculoPdf extends ConstructorDocumentacionVehiculo {

    public ConstructorDocumentacionVehiculoPdf() {
        documentacion = new DocumentoPdf();
    }

    @Override
    public void construyeSolicitudPedido(String nombreCliente) {
        StringBuilder documento = new StringBuilder();
        documento.append("<PDF>");
        documento.append("Solicitud  de pedido cliente");
        documento.append(nombreCliente);
        documento.append("</PDF>");
        documentacion.agregarDocumento(String.valueOf(documento));

    }

    @Override
    public void construyeSolicitudMatricula(String nombreSolicitante) {

        StringBuilder documento = new StringBuilder();
        documento.append("<PDF>");
        documento.append("Solicitud  de matricula  cliente");
        documento.append(nombreSolicitante);
        documento.append("</PDF>");
        documentacion.agregarDocumento(String.valueOf(documento));

    }
}
